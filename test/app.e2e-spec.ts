import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { AppModule } from '../src/app.module';
import { GameService } from '../src/services/game.service';
import { INestApplication } from '@nestjs/common';

describe('Game', () => {
  let app: INestApplication;
  const gameService = {
    getCards: () => [{ value: '1', suit: 'HEARTS' }],
    simulateGame: () => ({
      players: [{ name: 'mat' }, { name: 'ben' }],
      winner: 'mat',
    }),
  };

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [AppModule],
    })
      .overrideProvider(GameService)
      .useValue(gameService)
      .compile();

    app = moduleRef.createNestApplication();
    await app.init();
  });

  it(`/GET cards`, () => {
    return request(app.getHttpServer())
      .get('/cards')
      .expect(200)
      .expect(gameService.getCards());
  });

  it(`/GET game simulation`, () => {
    return request(app.getHttpServer())
      .get('/mat/ben')
      .expect(200)
      .expect(gameService.simulateGame());
  });

  afterAll(async () => {
    await app.close();
  });
});
