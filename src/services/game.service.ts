import { Injectable } from '@nestjs/common';
import { DeckRepository } from '../repositories/DeckRepository';
import { Player } from '../models/Player';
import { CardDealer } from '../models/CardDealer';
import { Game } from '../models/Game';
import { CardFromDB } from '../models/Card';

@Injectable()
export class GameService {
  constructor(private deckRepository: DeckRepository) {}

  async getCards(): Promise<CardFromDB[]> {
    return await this.deckRepository.getShuffledDeckOfCards();
  }

  async simulateGame(firstPlayerName, secondPlayerName) {
    const deck = await this.getCards();
    const dealer = new CardDealer(deck);
    const player1 = new Player(firstPlayerName);
    const player2 = new Player(secondPlayerName);

    const game = new Game([player1, player2], dealer);
    return game.simulate();
  }
}
