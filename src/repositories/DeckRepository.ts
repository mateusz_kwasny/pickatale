import { CardFromDB } from '../models/Card';
import { HttpService, Injectable } from '@nestjs/common';

@Injectable()
export class DeckRepository {
  constructor(private httpService: HttpService) {}

  async getShuffledDeckOfCards(): Promise<CardFromDB[]> {
    const response = await this.httpService
      .get('https://pickatale-backend-case.herokuapp.com/shuffle')
      .toPromise();
    return response.data;
  }
}
