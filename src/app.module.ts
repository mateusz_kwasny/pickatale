import { HttpModule, Module } from '@nestjs/common';
import { GameController } from './controllers/game.controller';
import { GameService } from './services/game.service';
import { ConfigModule } from '@nestjs/config';
import { DeckRepository } from './repositories/DeckRepository';

@Module({
  imports: [ConfigModule.forRoot(), HttpModule],
  controllers: [GameController],
  providers: [GameService, DeckRepository],
})
export class AppModule {}
