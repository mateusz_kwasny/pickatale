import { Card, CardFromDB } from './Card';

export class CardDealer {
  constructor(private deck: CardFromDB[]) {}

  giveCard(): Card | null {
    if (!this.deck.length) {
      return null;
    }
    return new Card(this.deck.shift());
  }
}
