import { Game } from '../Game';

import { CardDealer } from '../CardDealer';
import { Player } from '../Player';
import { SuitValue, CardValue } from '../Card';

describe('Game', () => {
  let player1;
  let player2;
  beforeEach(() => {
    player1 = new Player('Mat');
    player2 = new Player('Ben');
  });
  it('should return first player as a winner', () => {
    // given
    const cards = [
      { suit: 'HEARTS' as SuitValue, value: 'A' as CardValue },
      { suit: 'HEARTS' as SuitValue, value: 'K' as CardValue },
      { suit: 'HEARTS' as SuitValue, value: '2' as CardValue },
      { suit: 'HEARTS' as SuitValue, value: '3' as CardValue },
    ];
    const cardDealer = new CardDealer(cards);

    // when
    const game = new Game([player1, player2], cardDealer);
    const result = game.simulate();

    // then
    const expectedResult = {
      winner: 'Mat',
      players: [
        {
          points: 21,
          cards: ['HA', 'HK'],
          name: 'Mat',
        },
        {
          points: 5,
          cards: ['H2', 'H3'],
          name: 'Ben',
        },
      ],
    };
    expect(result).toEqual(expectedResult);
  });

  it('should return second player as a winner', () => {
    // given
    const cards = [
      { suit: 'HEARTS' as SuitValue, value: '8' as CardValue },
      { suit: 'HEARTS' as SuitValue, value: 'K' as CardValue },
      { suit: 'HEARTS' as SuitValue, value: 'A' as CardValue },
      { suit: 'HEARTS' as SuitValue, value: 'Q' as CardValue },
    ];
    const cardDealer = new CardDealer(cards);

    // when
    const game = new Game([player1, player2], cardDealer);
    const result = game.simulate();

    // then
    const expectedResult = {
      winner: 'Ben',
      players: [
        {
          points: 18,
          cards: ['H8', 'HK'],
          name: 'Mat',
        },
        {
          points: 21,
          cards: ['HA', 'HQ'],
          name: 'Ben',
        },
      ],
    };
    expect(result).toEqual(expectedResult);
  });

  it('should return first player as a winner with 3 cards', () => {
    // given
    const cards = [
      { suit: 'HEARTS' as SuitValue, value: '8' as CardValue },
      { suit: 'HEARTS' as SuitValue, value: '7' as CardValue },
      { suit: 'HEARTS' as SuitValue, value: 'K' as CardValue },
      { suit: 'HEARTS' as SuitValue, value: 'Q' as CardValue },
      { suit: 'HEARTS' as SuitValue, value: '6' as CardValue },
    ];
    const cardDealer = new CardDealer(cards);

    // when
    const game = new Game([player1, player2], cardDealer);
    const result = game.simulate();

    // then
    const expectedResult = {
      winner: 'Mat',
      players: [
        {
          points: 21,
          cards: ['H8', 'H7', 'H6'],
          name: 'Mat',
        },
        {
          points: 20,
          cards: ['HK', 'HQ'],
          name: 'Ben',
        },
      ],
    };
    expect(result).toEqual(expectedResult);
  });

  it('should return second player as a winner with 3 cards', () => {
    // given
    const cards = [
      { suit: 'HEARTS' as SuitValue, value: '8' as CardValue },
      { suit: 'HEARTS' as SuitValue, value: '7' as CardValue },
      { suit: 'HEARTS' as SuitValue, value: 'K' as CardValue },
      { suit: 'HEARTS' as SuitValue, value: '5' as CardValue },
      { suit: 'HEARTS' as SuitValue, value: '3' as CardValue },
      { suit: 'DIAMONDS' as SuitValue, value: '6' as CardValue },
    ];
    const cardDealer = new CardDealer(cards);

    // when
    const game = new Game([player1, player2], cardDealer);
    const result = game.simulate();

    // then
    const expectedResult = {
      winner: 'Ben',
      players: [
        {
          points: 18,
          cards: ['H8', 'H7', 'H3'],
          name: 'Mat',
        },
        {
          points: 21,
          cards: ['HK', 'H5', 'D6'],
          name: 'Ben',
        },
      ],
    };
    expect(result).toEqual(expectedResult);
  });
  it('should return second player as a winner because of too many points for first player', () => {
    // given
    const cards = [
      { suit: 'HEARTS' as SuitValue, value: '8' as CardValue },
      { suit: 'HEARTS' as SuitValue, value: '7' as CardValue },
      { suit: 'HEARTS' as SuitValue, value: 'K' as CardValue },
      { suit: 'HEARTS' as SuitValue, value: '5' as CardValue },
      { suit: 'HEARTS' as SuitValue, value: '10' as CardValue },
    ];
    const cardDealer = new CardDealer(cards);

    // when
    const game = new Game([player1, player2], cardDealer);
    const result = game.simulate();

    // then
    const expectedResult = {
      winner: 'Ben',
      players: [
        {
          points: 25,
          cards: ['H8', 'H7', 'H10'],
          name: 'Mat',
        },
        {
          points: 15,
          cards: ['HK', 'H5'],
          name: 'Ben',
        },
      ],
    };
    expect(result).toEqual(expectedResult);
  });
});
