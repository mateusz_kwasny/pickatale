import { blackJack, Player } from './Player';
import { CardDealer } from './CardDealer';

export class Game {
  winner: Player | null | { name: string };

  constructor(private players: [Player, Player], private dealer: CardDealer) {}

  simulate() {
    const [firstPlayer, secondPlayer] = this.players;
    this.drawCard(firstPlayer);
    this.drawCard(firstPlayer);

    this.drawCard(secondPlayer);
    this.drawCard(secondPlayer);

    if (!this.winner) {
      this.continueGameForPlayerOne(firstPlayer);
    }

    return {
      players: [firstPlayer, secondPlayer],
      winner: this.winner.name,
    };
  }

  drawCard(player: Player) {
    const nextCard = this.dealer.giveCard();
    player.takeCard(nextCard);
    if (player.hasBlackJack()) {
      this.winner = player;
    }
    if (player.hasLost()) {
      const indexOfCurrentPlayer = this.players.findIndex(
        ({ name }) => name === player.name,
      );
      this.winner =
        indexOfCurrentPlayer === 0 ? this.players[1] : this.players[0];
    }
  }

  continueGameForPlayerOne(player: Player) {
    while (player.shouldTakeNextCard()) {
      this.drawCard(player);
    }
    if (!this.winner) {
      this.continueGameForPlayerTwo(this.players[1]);
    }
  }

  continueGameForPlayerTwo(player: Player) {
    while (this.players[1].points < this.players[0].points) {
      this.drawCard(player);
    }
    if (!this.winner) {
      this.calculateWhoIsCloserToTwentyOne();
    }
  }

  calculateWhoIsCloserToTwentyOne() {
    const firstPlayerGap = blackJack - this.players[0].points;
    const secondPlayerGap = blackJack - this.players[1].points;

    if (firstPlayerGap === secondPlayerGap) {
      this.winner = { name: 'DRAW' };
    } else if (firstPlayerGap < secondPlayerGap) {
      this.winner = this.players[0];
    } else {
      this.winner = this.players[1];
    }
  }
}
