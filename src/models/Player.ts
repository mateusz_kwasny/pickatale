import { Card, CardInHand } from './Card';

export const blackJack = 21;
export const shouldTakeNextCardLimit = 17;

export class Player {
  points: number;
  cards: CardInHand[];

  constructor(public name: string) {
    this.cards = [];
    this.points = 0;
  }

  takeCard(card: Card) {
    this.cards.push(`${card.suit.charAt(0)}${card.value}`);
    this.points = this.points + card.getPointsValue();
  }

  hasLost() {
    return this.points > blackJack;
  }

  hasBlackJack() {
    return this.points === blackJack;
  }

  shouldTakeNextCard() {
    return this.points < shouldTakeNextCardLimit;
  }
}
