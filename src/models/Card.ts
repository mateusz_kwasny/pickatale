export type CardValue =
  | '2'
  | '3'
  | '4'
  | '5'
  | '6'
  | '7'
  | '8'
  | '9'
  | '10'
  | 'J'
  | 'Q'
  | 'K'
  | 'A';

export type SuitValue = 'HEARTS' | 'DIAMONDS' | 'SPADES' | 'CLUBS';

export type CardInHand = string;

export interface CardFromDB {
  suit: SuitValue;
  value: CardValue;
}

export class Card {
  suit: SuitValue;
  value: CardValue;

  constructor(card) {
    this.suit = card.suit;
    this.value = card.value;
  }

  getPointsValue(): number {
    switch (this.value) {
      case 'A':
        return 11;
      case 'K':
      case 'Q':
      case 'J':
        return 10;
      case '10':
      case '9':
      case '8':
      case '7':
      case '6':
      case '5':
      case '4':
      case '3':
      case '2':
        return Number(this.value);
    }
  }
}
