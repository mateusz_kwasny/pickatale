import { Test } from '@nestjs/testing';
import { GameController } from '../game.controller';
import { GameService } from '../../services/game.service';
import { Card } from '../../models/Card';
import { DeckRepository } from '../../repositories/DeckRepository';
import { HttpModule } from '@nestjs/common';

describe('GameController', () => {
  let gameController: GameController;
  let gameService: GameService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [HttpModule],
      controllers: [GameController],
      providers: [GameService, DeckRepository],
    }).compile();

    gameService = moduleRef.get<GameService>(GameService);
    gameController = moduleRef.get<GameController>(GameController);
  });

  describe('Return deck of cards', () => {
    it('should return an array of cards', async () => {
      // given
      const result = [
        new Card({ suit: 'HEARTS', value: '2' }),
        new Card({ suit: 'HEARTS', value: '1' }),
      ];
      jest
        .spyOn(gameService, 'getCards')
        .mockImplementation(() => Promise.resolve(result));

      // when
      const cards = await gameController.getCards();

      // then
      expect(cards).toBe(result);
    });
  });
});
