import { Controller, Get, Param } from '@nestjs/common';
import { GameService } from '../services/game.service';
import { CardFromDB } from '../models/Card';

@Controller()
export class GameController {
  constructor(private readonly gameService: GameService) {}

  @Get('/:firstPlayer/:secondPlayer')
  async getGameResult(
    @Param('firstPlayer') firstPlayer: string,
    @Param('secondPlayer') secondPlayer: string,
  ) {
    return await this.gameService.simulateGame(firstPlayer, secondPlayer);
  }
  @Get('/cards')
  async getCards(): Promise<CardFromDB[]> {
    return await this.gameService.getCards();
  }
}
