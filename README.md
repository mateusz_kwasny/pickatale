# Pickatale

## General info
Simple API service for returning a result of simulation of the Blackjack game

## Technologies
API is created with:

* Nest.js

## How to develop

### Clone this repository

```bash
$ git clone https://gitlab.com/mateusz_kwasny/pickatale
```

### Go into the repository

```bash
$ cd pickatale
```

## Installation

```bash
$ yarn install
```

## Running the app

```bash
# development
$ yarn run start:dev
```

## Test

```bash
# unit tests
$ yarn run test

# e2e tests
$ yarn run test:e2e
```

## Endpoints

The app is running locally on port 3000. There are two available endpoints: 

- http://localhost:3000/:playerOneName/:playerTwoName - returning result of the game for passed players

Output shape:

```json
{
    "players": [
        {
            "name": "mat",
            "cards": [
                "D3",
                "H3",
                "S10",
                "C6"
            ],
            "points": 22
        },
        {
            "name": "ben",
            "cards": [
                "D8",
                "SQ"
            ],
            "points": 18
        }
    ],
    "winner": "ben"
}
```

- http://localhost:3000/cards - returning deck of the cards

Output shape: 

```json
[
    {
        "suit": "SPADES",
        "value": "A"
    },
    {
        "suit": "HEARTS",
        "value": "J"
    },
    {
        "suit": "DIAMONDS",
        "value": "10"
    },
    {
        "suit": "CLUBS",
        "value": "4"
    }
]
```
